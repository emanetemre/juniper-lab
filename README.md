# Juniper Lab

https://www.juniper.net/us/en/dm/vjunos-labs.html

```
sudo apt-get update
sudo apt install qemu qemu-kvm libvirt-clients libvirt-daemon-system bridge-utils virt-manager
sudo apt install x11-apps

sudo systemctl enable --now libvirtd
sudo systemctl enable --now virtlogd

sudo apt install git
git clone https://github.com/DamionGans/ubuntu-wsl2-systemd-script.git
cd ubuntu-wsl2-systemd-script/
sudo bash ubuntu-wsl2-systemd-script.sh --force

```

Kurduktan sonra wsl'i shutdown ediyoruz.

```
PS C:\Users\Emre> wsl -l -v
  NAME                   STATE           VERSION
* kali-linux             Stopped         2
  docker-desktop         Stopped         2
  Ubuntu                 Running         2
  docker-desktop-data    Stopped         2
PS C:\Users\Emre> wsl --shutdown -d Ubuntu
PS C:\Users\Emre> wsl -l -v
  NAME                   STATE           VERSION
* kali-linux             Stopped         2
  docker-desktop         Stopped         2
  Ubuntu                 Stopped         2
  docker-desktop-data    Stopped         2
PS C:\Users\Emre>
```